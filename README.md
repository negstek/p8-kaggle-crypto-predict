# P8-kaggle-crypto-predict



## Getting started

This project present my research for [this kaggle competition](https://www.kaggle.com/c/g-research-crypto-forecasting)!

## First test

At first I make some test on gluonts (DeepAR), you can find it in test gluon repository

## First step

You should start in downloding competitions data [here](https://www.kaggle.com/c/g-research-crypto-forecasting/data)
Then unzip it in competition scripts repository and used scripts in jupyter.

Let's give a try :)

## Doc

I write some documentation in french about used algorithm, you can find it in doc repository.
